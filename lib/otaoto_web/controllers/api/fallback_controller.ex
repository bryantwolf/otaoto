defmodule OtaotoWeb.Api.FallbackController do
  use OtaotoWeb, :controller

  def call(conn, {:error, changeset = %Ecto.Changeset{}}) do
    conn
    |> put_status(:unprocessable_entity)
    |> render("errors.json", messages: changeset.errors)
  end

  def call(conn, {:error, :slug_not_found}) do
    conn
    |> put_status(:bad_request)
    |> render("errors.json", messages: ["No slug was supplied to locate the secret message."])
  end

  def call(conn, {:error, :key_not_found}) do
    conn
    |> put_status(:bad_request)
    |> render("errors.json", messages: ["No key was supplied to decrypt the secret message."])
  end

  def call(conn, {:error, :no_plain_text}) do
    conn
    |> put_status(:bad_request)
    |> render("errors.json", messages: ["No secret message was supplied to encrypt."])
  end

  def call(conn, {:error, _message}) do
    conn
    |> put_status(:not_found)
    |> render("errors.json", messages: ["This link has expired or never existed"])
  end
end
