defmodule OtaotoWeb.Api.SecretController do
  use OtaotoWeb, :controller
  require Logger
  action_fallback(OtaotoWeb.Api.FallbackController)

  alias Otaoto.Secret

  def create(conn, %{"secret" => raw_params}) do
    with {:ok, secret} <- Secret.create(raw_params["plain_text"]) do
      link = Routes.api_secret_url(conn, :show, secret.slug, key: secret.key)
      Logger.metadata(secret_id: secret.id, slug: secret.slug, source: :api)
      Logger.info("secret created")

      conn
      |> put_status(:created)
      |> render("create.json",
        secret: %{key: secret.key, slug: secret.slug, link: link}
      )
    end
  end

  def show(conn, %{"slug" => slug}) do
    with {:ok, key} <- get_key(conn),
         :ok <- confirm_existence_of(slug),
         {:ok, plain_text, secret} <- Secret.retrieve(slug, key) do
      Logger.metadata(secret_id: secret.id, slug: secret.slug, source: :api)
      Logger.info("secret viewed, data deleted")
      render(conn, "show.json", plain_text: plain_text)
    end
  end
end
