defmodule OtaotoWeb.Html.SecretController do
  use OtaotoWeb, :controller
  require Logger

  action_fallback(OtaotoWeb.Html.FallbackController)
  alias Otaoto.Secret

  def new(conn, _params) do
    render(conn, "new.html", changeset: Secret.changeset(%{}))
  end

  def create(conn, %{"secret" => params}) do
    plain_text = Map.get(params, "plain_text")

    with {:ok, secret} <- Secret.create(plain_text) do
      Logger.metadata(secret_id: secret.id, slug: secret.slug, source: :app)
      Logger.info("secret created")
      render(conn, "confirm.html", key: secret.key, slug: secret.slug, plain_text: plain_text)
    end
  end

  def gate(conn, %{"slug" => slug}) do
    with {:ok, key} <- get_key(conn) do
      Logger.metadata(slug: slug, source: :app)
      Logger.info("secret gate viewed")
      render(conn, "gate.html", slug: slug, key: key)
    end
  end

#  def gate(conn, %{"slug" => slug, "key" => key}) do
#    Logger.metadata(slug: slug, source: :app)
#    Logger.info("secret gate viewed")
#    render(conn, "gate.html", slug: slug, key: key)
#  end

  def show(conn, %{"slug" => slug}) do
    with {:ok, key} <- get_key(conn),
         :ok <- confirm_existence_of(slug),
         {:ok, plain_text, secret} <- Secret.retrieve(slug, key) do
      Logger.metadata(secret_id: secret.id, slug: secret.slug, source: :app)
      Logger.info("secret viewed, data deleted")
      render(conn, "show.html", plain_text: plain_text)
    end
  end

  def gone(conn, %{"slug" => slug}) do
    Logger.metadata(slug: slug, source: :app)
    Logger.info("failed attempt to find secret")
    render(conn, "gone.html")
  end

  def gone(conn, _params) do
    Logger.metadata(source: :app)
    Logger.info("failed attempt to find secret")
    render(conn, "gone.html")
  end
end
