defmodule OtaotoWeb.Html.FallbackController do
  use OtaotoWeb, :controller

  def call(conn, {:error, changeset = %Ecto.Changeset{}}) do
    conn
    |> put_flash(:error, "we could not create your secret")
    |> render("new.html", changeset: changeset)
  end

  def call(conn, {:error, _message}) do
    redirect(conn, to: Routes.secret_path(conn, :gone))
  end
end
