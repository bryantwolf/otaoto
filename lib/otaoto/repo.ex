defmodule Otaoto.Repo do
  use Ecto.Repo,
    otp_app: :otaoto,
    adapter: Ecto.Adapters.Postgres
end
