defmodule Otaoto.Schema do
  defmacro __using__(_) do
    quote do
      use Ecto.Schema
      alias Otaoto.Repo
      import Ecto.Changeset
      require Logger
      @primary_key {:id, Ecto.UUID, autogenerate: true}
      @foreign_key_type Ecto.UUID
      @derive Jason.Encoder
    end
  end
end
