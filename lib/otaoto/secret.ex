defmodule Otaoto.Secret do
  use Otaoto.Schema
  @slug_length Application.get_env(:once, :slug_length, 3)

  alias __MODULE__, as: Secret
  alias Bcrypt
  alias Otaoto.AES

  schema "secrets" do
    field(:cipher_text, :string)
    field(:slug, :string)
    field(:key_hash, :string)
    field(:key, :string, virtual: true)

    timestamps()
  end

  @required_fields ~w(cipher_text key)a

  def changeset(%Secret{} = schema \\ %Secret{}, params) do
    schema
    |> cast(params, @required_fields)
    |> hash_key()
    |> put_change(:slug, new_slug())
  end

  def create(nil), do: {:error, :no_plain_text}

  def create(plain_text) do
    plain_text
    |> AES.encrypt()
    |> Secret.changeset()
    |> Repo.insert()
  end

  def retrieve(slug, key) do
    with {:ok, secret} <- get(slug),
         {:ok, plain_text} <- AES.decrypt(secret.cipher_text, key) do
      Repo.delete(secret)

      {:ok, plain_text, secret}
    end
  end

  defp get(slug) do
    case Repo.get_by(Secret, slug: slug) do
      nil -> {:error, :invalid_slug_or_key}
      %Otaoto.Secret{} = secret -> {:ok, secret}
    end
  end

  defp new_slug do
    slug = MnemonicSlugs.generate_slug(@slug_length)

    Otaoto.Secret
    |> Repo.get_by(slug: slug)
    |> case do
      {:ok, _secret} -> new_slug()
      nil -> slug
    end
  end

  def hash_key(changeset) do
    changeset
    |> get_change(:key)
    |> case do
      nil -> changeset
      key -> put_change(changeset, :key_hash, Bcrypt.hash_pwd_salt(key))
    end
  end
end
