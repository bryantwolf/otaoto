defmodule Otaoto.AES do
  def encrypt(plain_text) do
    iv = new_iv()
    key = new_key()

    cipher_text =
      :aes_ctr
      |> :crypto.crypto_init(key, iv, false)
      |> :crypto.crypto_update(to_string(plain_text))

    iv_cipher_text = iv <> cipher_text

    %{cipher_text: Base.url_encode64(iv_cipher_text), key: Base.url_encode64(key)}
  end

  def decrypt(cipher_text_string, key_string) do
    case Base.url_decode64(key_string) do
      {:ok, key} ->
        case Base.url_decode64(cipher_text_string) do
          {:ok, iv_cipher} -> decipher(iv_cipher, key)
          :error -> {:error, "Expected cipher text to be a base64 encoded string"}
        end

      :error ->
        {:error, "Expected key to be a base64 encoded string"}
    end
  end

  defp decipher(iv_cipher, key) do
    <<iv::binary-16, cipher_text::binary>> = iv_cipher

    plain_text =
      :aes_ctr
      |> :crypto.crypto_init(key, iv, false)
      |> :crypto.crypto_update(cipher_text)

    {:ok, plain_text}
  end

  defp new_key, do: :crypto.strong_rand_bytes(32)
  defp new_iv, do: :crypto.strong_rand_bytes(16)
end
