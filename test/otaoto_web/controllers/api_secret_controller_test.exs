defmodule OtaotoWeb.Api.SecretControllerTest do
  use OtaotoWeb.ConnCase, async: true
  alias Otaoto.{Repo, Secret}

  describe "Creating a secret" do
    test "with plain text", %{conn: conn} do
      data = %{secret: %{plain_text: "hello, world."}}

      keys =
        conn
        |> post(Routes.api_secret_path(conn, :create, data))
        |> json_response(:created)
        |> Map.get("secret")
        |> Map.keys()

      assert "key" in keys
      assert "link" in keys
      assert "slug" in keys
      assert length(Repo.all(Secret)) == 1
    end

    test "if plain text is not included", %{conn: conn} do
      data = %{secret: %{not_plain_text: "hello, world."}}

      response =
        conn
        |> post(Routes.api_secret_path(conn, :create, data))
        |> json_response(:bad_request)

      assert(response["errors"] == ["No secret message was supplied to encrypt."])
      assert length(Repo.all(Secret)) == 0
    end
  end

  describe "retrieving a secret" do
    def req(conn, slug, key, json_response \\ :ok) do
      conn
      |> get(Routes.api_secret_path(conn, :show, slug, key: key))
      |> json_response(json_response)
    end

    test "for a valid key and slug", %{conn: conn} do
      plain_text = "Hello, World!"
      {:ok, secret} = Secret.create(plain_text)
      assert length(Repo.all(Secret)) == 1

      resp = req(conn, secret.slug, secret.key)
      assert resp["plain_text"] == plain_text

      assert length(Repo.all(Secret)) == 0
    end

    test "for a valid key but an invalid slug", %{conn: conn} do
      plain_text = "Hello, World!"
      {:ok, secret} = Secret.create(plain_text)
      assert length(Repo.all(Secret)) == 1

      resp = req(conn, "invalid_slug", secret.key, :not_found)
      assert(resp["errors"] == ["This link has expired or never existed"])

      assert length(Repo.all(Secret)) == 1
    end

    test "for an invalid key but a valid slug", %{conn: conn} do
      plain_text = "Hello, World!"
      {:ok, secret} = Secret.create(plain_text)
      assert length(Repo.all(Secret)) == 1

      resp = req(conn, secret.slug, "invalid_key", :not_found)
      assert(resp["errors"] == ["This link has expired or never existed"])

      assert length(Repo.all(Secret)) == 1
    end

    test "for a valid key and slug that have already been used", %{conn: conn} do
      plain_text = "Hello, World!"
      {:ok, secret} = Secret.create(plain_text)
      assert length(Repo.all(Secret)) == 1
      Secret.retrieve(secret.slug, secret.key)

      resp = req(conn, secret.slug, secret.key, :not_found)
      assert(resp["errors"] == ["This link has expired or never existed"])

      assert length(Repo.all(Secret)) == 0
    end

    test "for an invalid key and slug", %{conn: conn} do
      assert length(Repo.all(Secret)) == 0
      resp = req(conn, "invalid_slug", "invalid_key", :not_found)
      assert(resp["errors"] == ["This link has expired or never existed"])

      assert length(Repo.all(Secret)) == 0
    end
  end
end
