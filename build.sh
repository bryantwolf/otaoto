#!/usr/bin/env bash
echo "starting build.sh..."
# exit on error
set -o errexit

# Initial setup
echo "starting initial setup..."
mix deps.get --only prod
MIX_ENV=prod mix compile

# Compile assets
echo "compiling assets..."
MIX_ENV=prod mix assets.deploy

## Generates some nice helpers for the release like the migrate we'll run later
#echo "generating release helpers..."
MIX_ENV=prod mix phx.gen.release

# Build the release and overwrite the existing release directory
echo "building release..."
MIX_ENV=prod mix release --overwrite

# Run migrations
echo "running database migrations..."
#_build/prod/rel/otaoto/bin/otaoto eval "Otaoto.Release.migrate"
_build/prod/rel/otaoto/bin/migrate

echo "build.sh complete."
