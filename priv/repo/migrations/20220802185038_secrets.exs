defmodule Otaoto.Repo.Migrations.Secrets do
  use Ecto.Migration

  def change do
    create table(:secrets, primary_key: false) do
      add :id, :uuid, primary_key: true
      add :cipher_text, :string
      add :key_hash, :string, null: false
      add :slug, :string, unique: true

      timestamps()
    end

    create unique_index(:secrets, [:slug])
  end
end
