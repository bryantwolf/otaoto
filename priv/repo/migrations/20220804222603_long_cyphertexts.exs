defmodule Otaoto.Repo.Migrations.LongCyphertexts do
  use Ecto.Migration

  def change do
    alter table(:secrets) do
      modify :cipher_text, :text
    end
  end
end
